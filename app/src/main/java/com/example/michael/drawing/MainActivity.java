package com.example.michael.drawing;

import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.RectF;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.Random;

public class MainActivity extends AppCompatActivity {

    private ImageView img;
    private TextView txt;
    private Bitmap bmp;
    int imgw;
    int imgh;
    int amount = 200;
    int marg = 4;
    int x1, y1;
    int x2, y2;
    Random r = new Random();
    Paint pen = new Paint();
    Canvas cnv;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        img=(ImageView)findViewById(R.id.imageView);
        txt=(TextView)findViewById(R.id.info);
    }

    public void onClickedCreate(View v) {
        getImageRes();
        bmp = Bitmap.createBitmap(imgw, imgh, Bitmap.Config.ARGB_8888);
        findViewById(R.id.reset_btn).setEnabled(true);
        findViewById(R.id.line_btn).setEnabled(true);
        findViewById(R.id.rect_btn).setEnabled(true);
        findViewById(R.id.elipse_btn).setEnabled(true);
        txt.setText(getString(R.string.d_create));
        cnv = new Canvas(bmp);
    }

    public void onClickedErase( View v) {
        Canvas cnv = new Canvas(bmp);
        pen.setStyle(Paint.Style.STROKE);
        bmp.eraseColor(Color.argb(255,236,236,236));
        img.setImageBitmap(bmp);
        txt.setText(getString(R.string.d_reset));
    }

    public void onClickedLines(View v) {
        getImageRes();
        pen.setStrokeWidth(3);
        for(int i=0; i<amount; i++) {
            randomisePoints(pen);
            cnv.drawLine(x1,y1,x2,y2,pen);
        }
        img.setImageBitmap(bmp);
        txt.setText(getString(R.string.dl));
    }

    public void onClickedRects(View v) {
        getImageRes();
        pen.setStrokeWidth(3);
        for(int i=0; i<amount; i++) {
            randomisePoints(pen);
            cnv.drawRect(x1,y1,x2,y2,pen);
        }
        img.setImageBitmap(bmp);
        txt.setText(getString(R.string.dr));
    }

    public void onClickedElipses(View v) {
        getImageRes();
        pen.setStrokeWidth(3);
        for(int i=0; i<amount; i++) {
            randomisePoints(pen);
            RectF rectf = new RectF(x1,y1,x2,y2);
            cnv.drawOval(rectf,pen);
        }
        img.setImageBitmap(bmp);
        txt.setText(getString(R.string.de));
    }

    public void onClickedClose(View v) {
        finish();
    }

    public void getImageRes() {
        imgw = img.getWidth();
        imgh = img.getHeight();
    }

    public void randomisePoints(Paint pen) {
        pen.setARGB(255,r.nextInt(256), r.nextInt(256), r.nextInt(256));
        x1 = marg + r.nextInt(imgw - (marg<<1));
        y1 = marg + r.nextInt(imgh - (marg<<1));
        x2 = marg + r.nextInt(imgw - (marg<<1));
        y2 = marg + r.nextInt(imgh - (marg<<1));
    }
}
